const rectangleArea = require('..');

describe('rectangleArea', () => {
  it('should returns a number', async () => {
    const result = rectangleArea(1, 1);

    expect(typeof result).toBe('number');
  });

  it('should returns a correct rectange area', async () => {
    expect(rectangleArea(2, 2)).toBe(4);
    expect(rectangleArea(-5, -10)).toBe(50);
    expect(rectangleArea(1, 0)).toBe(0);
    expect(rectangleArea(100, 100)).toBe(10000);
    expect(rectangleArea(-10, 10)).toBe(-100);
  });

  it('should returns a NaN when arguments are not numbers', async () => {
    expect(rectangleArea('a', 'b')).toBe(NaN);
    expect(rectangleArea(1, 'b')).toBe(NaN);
    expect(rectangleArea(undefined, [])).toBe(NaN);
    expect(rectangleArea({}, {})).toBe(NaN);
  });
});
